from django import forms
from django.contrib import admin
from django.db import models

from .models import Factory
from .models import Children

class FactoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'max_children', 'lower_bound', 'upper_bound']
    list_filter = ['name', 'max_children']
    list_display_links = ['name', 'max_children']
    search_fields = ['name', 'max_children', 'lower_bound', 'upper_bound']


class ChildrenAdmin(admin.ModelAdmin):
    list_display = ['factory_id', 'value']


admin.site.register(Factory, FactoryAdmin)
admin.site.register(Children, ChildrenAdmin)
