from django import forms
from .models import Factory
import re


class FactoryForm(forms.ModelForm):
    class Meta:
        model = Factory

        fields = [
            "name",
            "max_children",
            "lower_bound",
            "upper_bound"
        ]

    def clean(self):
        cleansed = super(FactoryForm, self).clean()
        name = cleansed.get("name")
        max_children = cleansed.get("max_children")
        lower_bound = cleansed.get("lower_bound")
        upper_bound = cleansed.get("upper_bound")

        if not re.match(r'^([A-Za-z0-9_-]\s?)+$', name):
            raise forms.ValidationError("Special characters are not allowed for name!")

        if max_children > 15:
            raise forms.ValidationError(
                "Max children must not exceed 15!"
            )

        if lower_bound > upper_bound:
                raise forms.ValidationError(
                    "Lower bound cannot be greater than the upper bound!"
                )
