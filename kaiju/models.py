from django.db import models
from model_utils import FieldTracker


class Factory(models.Model):
    name = models.CharField(max_length=20, null=False, blank=False, unique=True)
    max_children = models.PositiveIntegerField(null=False, blank=False, default=1)
    lower_bound = models.IntegerField(null=False, blank=False)
    upper_bound = models.IntegerField(null=False, blank=False)
    tracker = FieldTracker()  # for tracking changes to Factory fields


class Children(models.Model):
    factory = models.ForeignKey(Factory, on_delete=models.CASCADE, related_name='children')  # Parent key
    value = models.IntegerField(null=False);
