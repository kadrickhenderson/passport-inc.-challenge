from channels import Group
from channels.sessions import channel_session, enforce_ordering
from .models import Factory


@channel_session
def ws_connect(message):
    message.reply_channel.send({"accept": True})
    Group('live_factory').add(message.reply_channel)
    message.channel_session['factory'] = 'live_factory'


@channel_session
def ws_receive(message):
    # Passing this as we will only be broadcasting updates from the server side
    pass


@channel_session
def ws_disconnect(message):
    Group('live_factory').discard(message.reply_channel)

