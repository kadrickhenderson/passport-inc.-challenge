from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.root_factories, name='root_factories'),
    url(r'^(?P<id>\d+)/$', views.factory_detail, name='factory_detail'),  # (?P<id>\d+) is a named group
    url(r'^create/$', views.create_factory, name='create_factory'),
    url(r'^(?P<id>\d+)/update/$', views.update_factory, name='update_factory'),
    url(r'^(?P<id>\d+)/regen/$', views.regenerate_children, name='regeneration'),
    url(r'^(?P<id>\d+)/delete/$', views.delete_factory, name='delete_factory'),
]
