from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.http import Http404
from django.http import HttpResponseRedirect
from django.contrib import messages  # Django messages framework to prompt successful updates and creations
from channels import Group
from kaiju.models import Factory
from kaiju.models import Children
from kaiju.forms import FactoryForm
from tzlocal import get_localzone
import random
import json
import datetime


def create_factory(request):
    factories = Factory.objects.all()
    children = Children.objects.all()
    form = FactoryForm(request.POST or None)

    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()

        random_list = randomizer(instance.pk)

        # Updating Factory children in a batch
        Children.objects.bulk_create(random_list)
        # Not serializable, needs to be converted to a list
        children = Children.objects.filter(factory_id=instance.pk).values_list('value', flat=True)
        children_list = list(children)

        Group('live_factory').send({
            "text": json.dumps({
                "action": "CREATE",
                "id": instance.id,
                "name": instance.name,
                "max_children": instance.max_children,
                "lower_bound": instance.lower_bound,
                "upper_bound": instance.upper_bound,
                "children": children_list
            })
        })

        messages.success(request, "Factory Created!")
        return HttpResponseRedirect(reverse('factory_detail', args=(instance.pk,)))

    return render(request, 'kaiju/factory_form.html', {
        'form': form,
        'factories': factories,
        'children': children
    })


def regenerate_children(request, id=None):
    try:
        factory = Factory.objects.get(id=id)
    except Factory.DoesNotExist:
        raise Http404('This is a nonsense Factory!')

    random_list = randomizer(factory.id)

    Children.objects.bulk_create(random_list)
    children = Children.objects.filter(factory_id=id).values_list('value', flat=True)
    children_list = list(children)

    Group('live_factory').send({
        "text": json.dumps({
            "action": "REGEN",
            "id": factory.id,
            "name": factory.name,
            "max_children": factory.max_children,
            "lower_bound": factory.lower_bound,
            "upper_bound": factory.upper_bound,
            "children": children_list
        })
    })

    messages.info(request, "Generated " + factory.name, extra_tags="message_tag")
    return HttpResponseRedirect(reverse('factory_detail', args=(factory.id,)))


def update_factory(request, id=None):
    try:
        factory = Factory.objects.get(id=id)
    except Factory.DoesNotExist:
        raise Http404('This is a nonsense Factory!')

    # This guy is for name change checks
    old_name = factory.name
    form = FactoryForm(request.POST or None, instance=factory)
    factories = Factory.objects.all()
    children = Children.objects.all()

    if form.is_valid():
        if factory.tracker.has_changed('max_children') or \
                factory.tracker.has_changed('lower_bound') or \
                factory.tracker.has_changed('upper_bound'):

            instance = form.save(commit=False)
            instance.save()
            random_list = randomizer(factory.id)

            Children.objects.bulk_create(random_list)
            children_queryset = Children.objects.filter(factory_id=instance.pk).values_list('value', flat=True)
            children_list = list(children_queryset)

            Group('live_factory').send({
                "text": json.dumps({
                    "action": "UPDATE",
                    "id": instance.id,
                    "old_name": old_name,
                    "name": instance.name,
                    "max_children": instance.max_children,
                    "lower_bound": instance.lower_bound,
                    "upper_bound": instance.upper_bound,
                    "children": children_list
                })
            })

            messages.success(request, "Modified " + factory.name, extra_tags="message_tag")
            return HttpResponseRedirect(reverse('factory_detail', args=(factory.id,)))

        else:
            instance = form.save(commit=False)
            instance.save()

            children_queryset = Children.objects.filter(factory_id=instance.pk).values_list('value', flat=True)
            children_list = list(children_queryset)

            Group('live_factory').send({
                "text": json.dumps({
                    "action": "UPDATE",
                    "id": instance.id,
                    "old_name": old_name,
                    "name": instance.name,
                    "max_children": instance.max_children,
                    "lower_bound": instance.lower_bound,
                    "upper_bound": instance.upper_bound,
                    "children": children_list
                })
            })

            messages.success(request, "Modified " + factory.name, extra_tags="message_tag")
            return HttpResponseRedirect(reverse('factory_detail', args=(factory.id,)))

    return render(request, 'kaiju/factory_form_update.html', {
        'form': form,
        'factory': factory,
        'factories': factories,
        'children': children
    })


def delete_factory(request, id):
    try:
        factory = Factory.objects.get(id=id)
    except Factory.DoesNotExist:
        raise Http404('This is a nonsense Factory!')

    factory.delete()

    localzone = get_localzone()
    demolition_datetime = '%s' % datetime.datetime.now(localzone).strftime('%m-%d-%Y %H:%M:%S')

    Group('live_factory').send({
        "text": json.dumps({
            "action": "DEMOLISH",
            "id": id,
            "name": factory.name,
            "demo_time": demolition_datetime
        })
    })

    messages.success(request, factory.name + " Demolished!", extra_tags="message_tag")
    return redirect('root_factories')


def factory_detail(request, id):
    try:
        factory = Factory.objects.get(id=id)
    except Factory.DoesNotExist:
        raise Http404('Factory doesn\'t exist!')

    factories = Factory.objects.all()
    children = Children.objects.all()

    return render(request, 'kaiju/factory_detail.html', {
        'factory': factory,
        'factories': factories,
        'children': children
    })


def root_factories(request):
    factories = Factory.objects.all()
    children = Children.objects.all()

    return render(request, 'kaiju/root_factories.html', {
        'factories': factories,
        'children': children
    })


def randomizer(fac_id):
    rand_gen_list = []
    factory = Factory.objects.get(id=fac_id)
    Children.objects.filter(factory_id=factory.id).delete()

    for i in range(factory.max_children):
        rand_gen_list.append(Children(factory_id=factory.id, value=random.randint(factory.lower_bound,
                                                                                  factory.upper_bound)))
    return rand_gen_list
