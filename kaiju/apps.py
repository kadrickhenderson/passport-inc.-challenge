from django.apps import AppConfig


class KaijuPortalConfig(AppConfig):
    name = 'kaiju'
