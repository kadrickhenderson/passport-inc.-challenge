$(document).ready(function() {
    setTimeout(function() {
        $('.messages').fadeOut('slow');
    }, 5000); //5 seconds

    // Delete on click
    $('.messages').click(function(){
        $('.messages').attr('style', 'display:none;');
    })
});