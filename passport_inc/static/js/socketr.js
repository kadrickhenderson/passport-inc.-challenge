$(function() {
        var ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
        var chatsock = new ReconnectingWebSocket(ws_scheme + '://' + window.location.host + "/wormhole" + window.location.pathname);

        chatsock.onmessage = function(message) {
            var data = JSON.parse(message.data);
            var childrn = data.children;


            if (data.action == 'CREATE'){
                // Creating factory record under root tree
                $('#tree_update').append(
                    '<li class="parent_li factory"><span title="Factory">' + '<a href="' + "/" + data.id + "/" + '">'
                    + '<i class="fa fa-cog" aria-hidden="true"></i> ' + data.name
                    + '</a></span>' + '<ul>');

                for(var i in childrn) {
                    $('#tree_update .factory:last-child ul').append(
                    '<li class="parent_li child"><span title="Child">'
                    + '<i class="fa fa-user" aria-hidden="true"></i> '
                    + childrn[i]
                    + '</span></li>');
                }

                $('#tree_update').append('</ul></li>');

                // Creating newly broadcasted factory if on root
                if(window.location.pathname.replace(/\//g, '') == ''){
                    $('#node_portal').append('<div class="root_factory_class">'
                        + '<p><i class="fa fa-cog fa-spin fa-fw" aria-hidden="true"></i></p>'
                        + '<p>Factory Name: ' + data.name + '</p>'
                        + '<p>Children: ' + data.max_children + '</p>'
                        + '<p>Lower Bound: ' + data.lower_bound + '</p>'
                        + '<p>Upper Bound: ' + data.upper_bound + '</p>'
                        + '<p><span title="Regenerate"><a href="' + "/" + data.id + '/regen/"><i class="fa fa-refresh" aria-hidden="true"></i></a></span></p>'
                        + '<p><span title="Modify"><a href="' + "/" + data.id + '/update/"><i class="fa fa-wrench" aria-hidden="true"></i></a></span></p>'
                        + '<p><span title="Children"><a href="' + "/" + data.id + '/"><i class="fa fa-users" aria-hidden="true"></i></a></span></p>'
                        + '<p><span title="Demolish"><a href="' + "/" + data.id + '/delete/"><i class="fa fa-times" aria-hidden="true"></i></a></span></p>'
                        + '</div>'
                    );
                }
            }else if (data.action == 'DEMOLISH'){
                // Remove demolished factory from Tree view
                $("#tree_view a").each(function(){
                    if($(this).text().trim() == data.name.trim()){
                        $(this).closest('li').remove();
                    }
                });

                // If User is viewing a factory currently being demolished, render demolition datetimestamp
                if(data.id == window.location.pathname.replace(/\//g, '') ||
                    data.id + "update" == window.location.pathname.replace(/\//g, '')){
                        $('#node_portal').empty();
                        $('#node_portal').css({'text-align' : 'center', 'font-size' : '15px', 'padding-top' : '15px'});
                        $('#node_portal img').css('display','block');
                        $('#node_portal').prepend('<img id="demolition" src="/static/image/demolition.svg" height="280px" style="margin-top:20px" />');
                        $('#node_portal').append("<p> Factory: " + data.name + " was scheduled for demolition on: " + data.demo_time + "</p>");
                        $('#node_portal p').css({'margin-top' : '15px', 'font-size' : '19px', 'color' : '#d9534f'});
                        $('#node_portal').append('<a href="/" style="color:#333"><i class="fa fa-cogs fa-2x" aria-hidden="true"></i></a>');
                }else if (window.location.pathname.replace(/\//g, '') == ''){
                     // If User is viewing root view, remove factory from view
                     $('.root_factory_class p').each(function(){
                        if($(this).html().trim() == "Factory Name: " + data.name.trim()){
                            $(this).closest('div').remove();
                        }
                     });
                }
            }else if (data.action == 'REGEN'){
                // Adding new children for regenerated factory on root tree
                $('#tree_update a').each(function(i){
                    if($(this).text().trim() == data.name.trim()){
                        // Removing old Children
                        $(this).closest('li.factory').children('ul').remove();
                        $("<ul>").insertAfter($(this).closest('span'));

                        // Updating new Children
                        for(var c in childrn) {
                            $(this).closest('li.factory').children('ul').append(
                            '<li class="parent_li child"><span title="Child">'
                            + '<i class="fa fa-user" aria-hidden="true"></i> '
                            + childrn[c]
                            + '</span></li>');
                        }
                    }
                });

                // If User is viewing a factory currently being refreshed, update children
                if(data.id == window.location.pathname.replace(/\//g, '') ||
                    data.id + "update" == window.location.pathname.replace(/\//g, '')){
                        $('.child_value').empty();

                        // Updating new Children
                        $('.child_group .child_class').each(function(i){
                            $(this).append('<p class="child_value">' + childrn[i] + '</p>')
                        })
                }
            }else if (data.action == 'UPDATE'){
                // Adding new name and child values for updated factory on root tree
                $('#tree_update a').each(function(i){
                    if($(this).text().trim() == data.old_name.trim()){
                        $(this).html('<i class="fa fa-cog" aria-hidden="true"></i> ' + data.name);
                        // Removing old Children
                        $(this).closest('li.factory').children('ul').remove();
                        $("<ul>").insertAfter($(this).closest('span'));

                        // Updating new Children
                        for(var c in childrn) {
                            $(this).closest('li.factory').children('ul').append(
                            '<li class="parent_li child"><span title="Child">'
                            + '<i class="fa fa-user" aria-hidden="true"></i> '
                            + childrn[c]
                            + '</span></li>');
                        }
                    }
                });

                // Updating newly broadcasted factory if on root
                if(window.location.pathname.replace(/\//g, '') == ''){
                        // If User is viewing root view, update factory
                      $('.root_factory_class p').each(function(){
                            if($(this).html().trim() == "Factory Name: " + data.name.trim() ||
                                $(this).html().trim() == "Factory Name: " + data.old_name.trim()){

                                    $(this).closest('div').empty().append(
                                        '<p><i class="fa fa-cog fa-spin fa-fw" aria-hidden="true"></i></p>'
                                        + '<p>Factory Name: ' + data.name + '</p>'
                                        + '<p>Children: ' + data.max_children + '</p>'
                                        + '<p>Lower Bound: ' + data.lower_bound + '</p>'
                                        + '<p>Upper Bound: ' + data.upper_bound + '</p>'
                                        + '<p><span title="Regenerate"><a href="' + "/" + data.id + '/regen/"><i class="fa fa-refresh" aria-hidden="true"></i></a></span></p>'
                                        + '<p><span title="Modify"><a href="' + "/" + data.id + '/update/"><i class="fa fa-wrench" aria-hidden="true"></i></a></span></p>'
                                        + '<p><span title="Children"><a href="' + "/" + data.id + '/"><i class="fa fa-users" aria-hidden="true"></i></a></span></p>'
                                        + '<p><span title="Demolish"><a href="' + "/" + data.id + '/delete/"><i class="fa fa-times" aria-hidden="true"></i></a></span></p>'
                                    );
                            }
                      });

                // If User is viewing a factory currently being updated, update fields and children
                }else if(data.id == window.location.pathname.replace(/\//g, '')){
                        $('.factory_class').empty();
                        $('.child_group').empty();

                        $('.factory_class').append('<p><i class="fa fa-cog fa-spin fa-fw" aria-hidden="true"></i></p>'
                            + '<p>Factory Name: ' + data.name + '</p>'
                            + '<p>Children: ' + data.max_children + '</p>'
                            + '<p>Lower Bound: ' + data.lower_bound + '</p>'
                            + '<p>Upper Bound: ' + data.upper_bound + '</p>'
                            + '<p><span title="Regenerate"><a href="' + "/" + data.id + '/regen/"><i class="fa fa-refresh" aria-hidden="true"></i></a></span></p>'
                            + '<p><span title="Modify"><a href="' + "/" + data.id + '/update/"><i class="fa fa-wrench" aria-hidden="true"></i></a></span></p>'
                            + '<p><span title="Demolish"><a href="' + "/" + data.id + '/delete/"><i class="fa fa-times" aria-hidden="true"></i></a></span></p>'
                            + '</div>'
                        );

                        for(var c in childrn) {
                            $('.child_group').append('<div class="child_class">'+
                            '<i class="fa fa-user-circle" id="child_ico" aria-hidden="true"></i>'
                            + '<p class="child_value"> '
                            + childrn[c]
                            + '</p></div>');
                        }

                // If User is updating a factory currently being updated, update children
                }else if(data.id + "update" == window.location.pathname.replace(/\//g, '')){
                        $('.child_group').empty();

                        for(var c in childrn) {
                            $('.child_group').append('<div class="child_class">'+
                            '<i class="fa fa-user-circle" id="child_ico" aria-hidden="true"></i>'
                            + '<p class="child_value"> '
                            + childrn[c]
                            + '</p></div>');
                        }
                }
            };
        };

});