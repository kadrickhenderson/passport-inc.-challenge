$(document).ready(function () {

    //    We want to save the coordinates of the tree view when refresh
    var tree_coordinates = document.querySelector('#tree_view');

    $(function() {
        if (localStorage["x_co"] != undefined && localStorage["y_co"] != undefined) {
            tree_coordinates.scrollLeft = localStorage.getItem("x_co");
            tree_coordinates.scrollTop = localStorage.getItem("y_co");;
         }
    });

    $('#tree_view').scroll(function() {
        x_co = $('#tree_view').scrollLeft();
        y_co = $('#tree_view').scrollTop();
        localStorage.setItem("x_co", x_co);
        localStorage.setItem("y_co", y_co);

    });
});


