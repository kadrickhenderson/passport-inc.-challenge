# routing.py
from channels.routing import route
from kaiju.consumers import ws_connect, ws_disconnect, ws_receive
from django.conf.urls import include, url


channel_routing = [
    route('websocket.connect', ws_connect),
    route('websocket.receive', ws_receive),
    route('websocket.disconnect', ws_disconnect),
]

routing = [
    url(r"^/wormhole", channel_routing),
]

